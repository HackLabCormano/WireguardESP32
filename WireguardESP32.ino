// https://randomnerdtutorials.com/esp32-built-in-oled-ssd1306/

// https://github.com/tailscale/tailscale/issues/5220
// (tailscale, NO) tskey-auth-kJqasL7CNTRL-8UsJPWn8Mgdtvu4bW5ergdqwoxyvgWdmG

#include <WiFi.h>
#include <WireGuard-ESP32.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <ESPping.h>

#include "secrets.h"

int endpoint_port = 51820;  // [Peer] Endpoint

static constexpr const uint32_t UPDATE_INTERVAL_MS = 1000;
static WireGuard wg;

WiFiUDP ntpUDP;
// By default 'pool.ntp.org' is used with 60 seconds update interval and
// no offset
NTPClient timeClient(ntpUDP);

void pinga() {
  // Ping IP
  const IPAddress remote_ip(10, 100, 0, 1);
  //const IPAddress remote_ip(192, 168, 10, 1);
  //const IPAddress remote_ip(195, 201, 250, 108);  //NO?!?
  //const IPAddress remote_ip(192,168,142,1); //OK (locale)
  //const IPAddress remote_ip(138, 201, 158, 35);  // OK (eth server)

  Serial.print(remote_ip);
  if (Ping.ping(remote_ip, 3) > 0) {
    Serial.printf(" response time : %d/%.2f/%d ms\n", Ping.minTime(), Ping.averageTime(), Ping.maxTime());
  } else {
    Serial.println(" Error !");
  }
}

void setup() {
  esp_log_level_set("*", ESP_LOG_DEBUG);

  Serial.begin(115200);
  Serial.println("Connecting to the AP...");
  WiFi.begin(ssid, password);
  while (!WiFi.isConnected()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
  Serial.println("connected!");


  Serial.println("WiFi Connected.");

  Serial.println("Adjusting system time...");
  //configTime(9 * 60 * 60, 0, "ntp.jst.mfeed.ad.jp", "ntp.nict.jp", "time.google.com");
  timeClient.begin();
  timeClient.update();

  Serial.println("***");
  Serial.println("ping before wg");
  pinga();

  delay(UPDATE_INTERVAL_MS);
}

void loop() {
  Serial.println("-------------------------------");
  Serial.println(timeClient.getFormattedTime());  // UTC
  Serial.println(WiFi.localIP());

  if (!wg.is_initialized()) {
    Serial.println("Initializing WG interface...");
    if (!wg.begin(
          local_ip,
          Subnet,
          Gateway,
          private_key,
          endpoint_address,
          public_key,
          endpoint_port)) {
      Serial.println("Failed to initialize WG interface.");
    } else {
      Serial.print(">>> wg state: ");
      Serial.println(wg.is_initialized());
    }
  }

  Serial.println("***");
  Serial.println("ping after wg");
  pinga();

  delay(UPDATE_INTERVAL_MS);
}
